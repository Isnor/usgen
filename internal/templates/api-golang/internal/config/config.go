package config

import "usgen/template/internal/model"

type UsConfig struct {
	// {{if .Database.Enabled }}
	Database struct {
		// {{if .Database.PostgreSQL.Enabled }}
		PostgreSQL model.PostgresConfig
		// {{- end}}
	}
	// {{- end}}
}
