package service

import (
	"context"
	"usgen/template/internal/model"

	"github.com/jackc/pgx/v4"
)

// {{if .Database.Enabled }}
type DatabaseService interface {
	PutData(context.Context, *model.MockDatabaseData) error
	GetData(context context.Context, email string) (*model.MockDatabaseData, error)
}

// {{if .Database.PostgreSQL.Enabled }}
type PostgresService struct {
	model.PostgresConfig
	DB *pgx.Conn
}

func (d *PostgresService) PutData(ctx context.Context, data *model.MockDatabaseData) error {
	return d.DB.QueryRow(ctx, "INSERT INTO customers(id,email) VALUES($1, $2)", data.ID, data.Email).Scan()
}

func (d *PostgresService) GetData(ctx context.Context, email string) (*model.MockDatabaseData, error) {
	res := &model.MockDatabaseData{}
	err := d.DB.QueryRow(ctx, "SELECT id,email FROM customers WHERE email=$1", email).Scan(&res.ID, &res.Email)

	return res, err
}

// {{- end }}

func GetSQLDatabaseService(cfg model.PostgresConfig) DatabaseService {
	// {{if .Database.PostgreSQL.Enabled }}
	return &PostgresService{
		PostgresConfig: cfg,
	}
	// {{ else }}
	// 	log.Fatal("Database was enabled in template config without a corresponding implementation")
	// {{- end}}
}

// {{- end }}
