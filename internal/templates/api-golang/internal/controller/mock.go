package controller

import (
	"net/http"

	"github.com/go-chi/chi/v5"
)

type MockController struct{}

func (m *MockController) Router() http.Handler {
	router := chi.NewRouter()

	router.Get("/", m.mockEndpoint)
	return router
}

func (m *MockController) mockEndpoint(w http.ResponseWriter, r *http.Request) {

	w.WriteHeader(200)
	w.Write([]byte("hello, mock!"))
}
