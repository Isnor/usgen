package controller

import (
	"net/http"
)

type Controller interface {
	Router() http.Handler
}
