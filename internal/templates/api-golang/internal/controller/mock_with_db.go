package controller

import (
	"fmt"
	"net/http"
	"usgen/template/internal/service"

	"github.com/go-chi/chi/v5"
	"github.com/rs/zerolog/log"
)

type MockControllerWithDatabase struct {
	DataService service.DatabaseService
}

func (m *MockControllerWithDatabase) Router() http.Handler {
	router := chi.NewRouter()

	router.Get("/", m.mockDatabaseServiceEndpoint)
	return router
}

func (m *MockControllerWithDatabase) mockDatabaseServiceEndpoint(w http.ResponseWriter, r *http.Request) {


	data, err := m.DataService.GetData(r.Context(), "foo@bar.com")
	if err != nil {
		log.Err(err).Msg("Failed to get data from database")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("database failed"))
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(fmt.Sprintf("hello, mock! %+v\n", data)))
}
