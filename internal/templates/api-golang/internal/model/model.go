package model

// {{if .Database.Enabled }}

type MockDatabaseData struct {
	ID    string
	Email string
}

// {{if .Database.PostgreSQL.Enabled }}
type PostgresConfig struct {
	Username string `envconfig:"POSTGRES_USERNAME" required:"true"`
	Password string `envconfig:"POSTGRES_PASSWORD" required:"true"`
	Host     string `envconfig:"POSTGRES_HOST" required:"true"`
	Database string `envconfig:"POSTGRES_DATABASE" required:"true"`
}

// {{- end}}

// {{- end}}
