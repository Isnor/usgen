module usgen/template

go 1.15

require (
	github.com/go-chi/chi/v5 v5.0.0
	github.com/jackc/pgx/v4 v4.10.1
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/rs/zerolog v1.20.0
)
