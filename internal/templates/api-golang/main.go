package main

import (
	"context"
	"fmt"
	"net/http"

	"usgen/template/internal/config"
	"usgen/template/internal/controller"
	"usgen/template/internal/service"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/jackc/pgx/v4"
	"github.com/kelseyhightower/envconfig"
	"github.com/rs/zerolog/log"
)

func main() {
	ctx := context.Background()
	// configure
	usConfig := config.UsConfig{}
	envconfig.MustProcess("usgen", &usConfig)

	log.Warn().Msgf("config: %+v", usConfig)
	// create services
	// {{if .Database.Enabled }}

	// {{if .Database.PostgreSQL.Enabled }}
	// try to connect to postgres given the configuration
	pgConfig := usConfig.Database.PostgreSQL
	conn, err := pgx.Connect(ctx, fmt.Sprintf("postgresql://%s:%s@%s/%s", pgConfig.Username, pgConfig.Password, pgConfig.Host, pgConfig.Database))
	if err != nil {
		log.Fatal().Err(err).Msg("Could not connect to postgres")
		return
	}
	defer conn.Close(ctx)
	dbService := &service.PostgresService{
		PostgresConfig: pgConfig,
		DB:                              conn,
	}
	// {{- end}}

	// TODO: add other DB implementations here
	// {{- end}}

	// // create controllers
	// {{if .Database.Enabled }}
	mockWithDatabaseController := controller.MockControllerWithDatabase{
		DataService: dbService,
	}
	// {{- end }}
	mockController := &controller.MockController{}

	// create router
	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	// add controllers to the router
	r.Mount("/mock", mockController.Router())
	r.Mount("/db-mock", mockWithDatabaseController.Router())

	// create HTTP server
	end := make(chan error, 1)
	go func() {
		end <- http.ListenAndServe(":9000", r)
	}()

	log.Info().Msg("µsgen-sample started on port 9000.")

	if res := <-end; res != nil {
		log.Fatal().Err(res).Msg("we fucked up")
	}
}
