module isnor.ca/us_gen

go 1.13

require (
	github.com/nxadm/tail v1.4.8 // indirect
	github.com/onsi/ginkgo v1.15.0
	github.com/onsi/gomega v1.10.5
	github.com/rs/zerolog v1.20.0
	github.com/urfave/cli v1.22.5
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
)
