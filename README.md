# µsgen

`µsgen` is a command line tool for generating various microservice source code examples.

## Goals

The goal of `µsgen` was originally to remove the boilerplate/duplicated code I found myself writing when creating APIs in Go. Basically, writing a main function that uses some web framework to register web handlers and making it testable. Now I'd like to extend that to creating a Dockerfile and eventually a `Helm` chart. I don't think this will be particularly useful, but it should be good fun and practice.

## Usage

Use `usgen help` to view the commands the CLI supports. The main command is `create`. `usgen create help` will list the different types of microservices that can be generated and `usgen create <type> help` will show the help section for that type of microservice.

## Installing

The application can be installed with `go 1.13+` or built from source.

### Install with `Go`

`go install gitlab.com/Isnor/usgen/cmd/usgen`

### Build from source

`make cmd` with `make` or `go build -o usgen ./cmd/usgen` with `go` directly.

## Examples

`./usgen create --directory foobar go-api --database postgres`

Will create a directory named `foobar/api-golang` with some sample source code. This example creates an API that relies on connecting to a postgres instance so running it will fail without a valid configuration pointing to a running postgres server. To run the example, the `<api-directory>/deploy/docker-compose.yml` is created. Run it like any other `docker-compose`:

```
cd foobar/api-golang/deploy
docker-compose up
```

## TODO:
- [ ] uncomment/enable templates
- [ ] database in k8s
- [ ] real database code template
- [ ] test what happens with different configs
- [ ] add godoc and swagger (optional?) in template
- [ ] add Makefile target in template to test k8s service
- [ ] add a Dockerfile for usgen and build & push an image to gitlab image repo for releases