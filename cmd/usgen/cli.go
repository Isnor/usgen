package main

import (
	"os"

	"github.com/urfave/cli"
	"isnor.ca/us_gen/pkg/model"
	"isnor.ca/us_gen/pkg/usgen"
)

type CommandLine struct {
	registeredTemplates map[string]*model.DirectoryTemplate
}

func MustLoadDirectoryTemplateFromPath(path string) *model.DirectoryTemplate {
	apiTemplate, err := usgen.LoadDirectoryTemplateFromPath(path)
	if err != nil {
		// TODO: do something better here. The program should stop, but the CLI help should also be printed
		// TODO: if no templates could be found, there's a different error, and that should be a panic(?)
		panic("failed to load integration: api-golang")
	}
	return apiTemplate
}

func NewCommandLine() *CommandLine {

	integrations := make(map[string]*model.DirectoryTemplate)
	// TODO: these directories are loaded relative to the working directory where the command is run
	// What I want
	for integrationName, directory := range map[string]string{
		"api-golang": "internal/templates/api-golang",
	} {
		integrations[integrationName] = MustLoadDirectoryTemplateFromPath(directory)
	}

	return &CommandLine{
		registeredTemplates: integrations,
	}
}

func (c *CommandLine) isIntegrationRegistered(integrationName string) bool {
	_, found := c.registeredTemplates[integrationName]
	return found
}

// used this when I was trying to use `flag` package, not used in the current plan
// type CLIIntegration struct {
// 	Name        string
// 	Integration *model.Integration
// }

type CLIIntegration interface {
	ToCommand() cli.Command
	// Integration() *model.Integration
}

type GolangAPIIntegration struct {
	integration *model.DirectoryTemplate
	config      GoAPIIntegrationConfigBuilder
}

// func (i *GolangAPIIntegration) Integration() *model.Integration {
// 	return i.integration
// }

func (i *GolangAPIIntegration) ToCommand() cli.Command {

	return cli.Command{
		Name:  "go-api",
		Usage: "create a golang API",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:        "database",
				Usage:       "the database backend to use",
			},
		},
		Action: func(ctx *cli.Context) {
			i.config.Database = ctx.String("database")
			templateConfig := i.config.Build()

			usgen.WriteDirectoryTemplate(i.integration, &templateConfig, ctx.GlobalString(directoryFlag))
		},
	}
}

func (c *CommandLine) ParseArgs() {

	app := cli.NewApp()

	goAPIIntegration := GolangAPIIntegration{
		integration: c.registeredTemplates["api-golang"],
	}

	createMicroservice := &cli.Command{
		Name:    "create",
		Aliases: []string{"w"},
		Usage:   "create a microservice",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     directoryFlag,
				Usage:    "The directory to write the microservice to",
				Required: true,
			},
		},
		Subcommands: []cli.Command{
			goAPIIntegration.ToCommand(),
		},
		Action: func(ctx *cli.Context) {
			cli.DefaultAppComplete(ctx)
		},
	}

	/*
		create # command
		  <integration_name> # command
		    --database postgres # flag

	*/
	app.Commands = append(app.Commands, *createMicroservice)

	// parse args
	err := app.Run(os.Args)
	if err != nil {
		panic(err)
	}
}
