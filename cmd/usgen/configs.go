package main

import (
	"github.com/rs/zerolog/log"
	"isnor.ca/us_gen/pkg/model"
)

type GoAPIIntegrationConfig struct {
	Database GoAPIDatabaseConfig
}

type GoAPIDatabaseConfig struct {
	Enabled    bool
	PostgreSQL GoAPIPostgresConfig
}

type GoAPIPostgresConfig struct {
	Enabled bool
}

type GoAPIIntegrationConfigBuilder struct {
	DatabaseEnabled bool
	Database        string
}

func (g *GoAPIIntegrationConfigBuilder) Build() model.SourceCodeConfig {
	res := &GoAPIIntegrationConfig{}

	log.Info().Msg("building config")
	if g.Database != "" {
		res.Database.Enabled = true
		log.Info().Msg("database enabled")
		switch g.Database {
		case "postgres":
			res.Database.PostgreSQL.Enabled = true
			log.Info().Msg("postgres enabled")
		default:
			log.Fatal().Str("provided", g.Database).Msg("unrecognized database provided to go API integration")
		}
	}
	return res
}
