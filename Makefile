TEST_DIR=foobar

cmd:
	go build -o usgen ./cmd/usgen

test-cmd: cmd
	./usgen create --directory $(TEST_DIR) go-api --database postgres

clean:
	rm -rf $(TEST_DIR)
	rm usgen

.PHONY: cmd test clean