package model

import (
	"text/template"
)

type Integration struct {
	SourceCode *SourceCode
	// Features   []Feature
	// Config IntegrationConfig
}

// templates for source code, package.json (?), tests
// type SourceCode interface {
// 	FillCodeTemplates() []template.Template
// }

// type IntegrationConfig interface {
// 	GetSourceCodeConfig() SourceCodeConfig
// }

type SourceCodeConfig interface{}

// type Feature struct {}
// type Kubernetes struct {}

type FileTemplate struct {
	Name     string // relative file path in its parent DirectoryTemplate
	Template *template.Template
}

type DirectoryTemplate struct {
	Name        string
	Files       []FileTemplate
	Directories []DirectoryTemplate
}

type SourceCode struct {
	*DirectoryTemplate
	SourceCodeConfig
	// features?
}
