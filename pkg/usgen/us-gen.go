package usgen

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sync"
	"text/template"

	"github.com/rs/zerolog/log"
	"isnor.ca/us_gen/pkg/model"
)

// WriteDirectoryTemplate writes the directories and files of `template` into `rootDir`.
// `rootDir` must be an absolute path that already exists
func WriteDirectoryTemplate(tmpl *model.DirectoryTemplate, config model.SourceCodeConfig, rootDir string) error {
	// TODO: maybe add a check to ensure rootDir exists?
	// create "this" directory
	err := os.MkdirAll(fmt.Sprintf("%s/%s", rootDir, tmpl.Name), 0775)
	if err != nil {
		return err
	}

	// return supplied file relative to "this" directory: rootDir/template.Name/
	thisDir := func(name string) string {
		return fmt.Sprintf("%s/%s/%s", rootDir, tmpl.Name, name)
	}

	// write the files in the current directory of the template
	writeFiles := sync.WaitGroup{}
	writeFiles.Add(len(tmpl.Files))
	for _, tf := range tmpl.Files {
		// TODO: how should we handle these errors?
		go func(t model.FileTemplate) {
			defer writeFiles.Done()
			f, err := os.Create(thisDir(t.Name))
			if err != nil {
				log.Error().Err(err).Msg("Failed writing file template")
			}
			defer f.Close()
			if t.Template != nil {
				// TODO: use the actual config
				err = t.Template.Execute(f, config)
				if err != nil {
					log.Error().Err(err).Msg("Failed executing file template")
				}
			}
		}(tf)
	}
	writeFiles.Wait()

	// recurse through the directories of the directory template
	writeDirectories := sync.WaitGroup{}
	writeDirectories.Add(len(tmpl.Directories))
	for _, d := range tmpl.Directories {
		go func(t model.DirectoryTemplate, name string) {
			defer writeDirectories.Done()
			err := WriteDirectoryTemplate(&t, config, thisDir(""))
			if err != nil {
				log.Error().Err(err).Msg("Failed writing directory template")
			}
		}(d, d.Name)
	}
	writeDirectories.Wait()

	return nil
}

// func WriteIntegration(integration *model.Integration, rootDir string)  error{
// 	return WriteDirectoryTemplate(integration.SourceCode.DirectoryTemplate, &integration.SourceCode.SourceCodeConfig, rootDir)
// }

// LoadDirectoryTemplateFromPath loads the directories and files under `dir/` into a
// DirectoryTemplate. `dir` should be an absolute path
func LoadDirectoryTemplateFromPath(dir string) (*model.DirectoryTemplate, error) {

	res := &model.DirectoryTemplate{
		Name: filepath.Base(dir),
	}

	var recurseDirectoryTemplate func(string, *model.DirectoryTemplate) error
	recurseDirectoryTemplate = func(currentDir string, tmpl *model.DirectoryTemplate) error {
		files, err := ioutil.ReadDir(currentDir)
		if err != nil {
			log.Err(err).Str("dir", currentDir).Msg("Failed reading directory")
			return err
		}
		for _, f := range files {
			path := filepath.Join(currentDir, f.Name())
			if !f.IsDir() {
				t, err := template.ParseFiles(path)
				if err != nil {
					log.Err(err).Str("template path", path).Msg("Failed parsing template")
					return err
				}
				// add the files in currentDir to tmpl
				tmpl.Files = append(tmpl.Files, model.FileTemplate{
					Name:     f.Name(),
					Template: t,
				})
			} else {
				// recurse and add the directories to tmpl
				t := &model.DirectoryTemplate{
					Name: f.Name(),
				}
				err := recurseDirectoryTemplate(path, t)
				if err != nil {
					log.Err(err).Msg("Failed recursing directory template")
					return err
				}
				tmpl.Directories = append(tmpl.Directories, *t)
			}
		}

		return nil
	}
	if err := recurseDirectoryTemplate(dir, res); err != nil {
		log.Err(err).Str("root dir", dir).Msg("Failed loading directory template")
		return res, err
	}
	return res, nil
}