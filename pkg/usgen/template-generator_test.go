package usgen_test

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
	"text/template"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"isnor.ca/us_gen/pkg/model"
	"isnor.ca/us_gen/pkg/usgen"
)

var testDir string
var testFileTemplate *template.Template
var testTemplate *model.DirectoryTemplate

const (
	testFileTemplatePath = "../../test/templates/files/test_file.tmpl"
	testDirTemplatePath  = "../../test/templates/dirs/simple-test"
)

// TODO: move these and the shared vars to a _suite_test.go file?
var _ = BeforeSuite(func() {
	var err error
	testDir, err = ioutil.TempDir(os.TempDir(), "usgen-*")

	if err != nil {
		Fail(err.Error())
	}

	testFileTemplate, err = template.ParseFiles(testFileTemplatePath)

	if err != nil {
		Fail(err.Error())
	}

	testTemplate = &model.DirectoryTemplate{
		Name: "simple-test",
		Files: []model.FileTemplate{
			{
				Name: "test_file",
			},
		},
		Directories: []model.DirectoryTemplate{
			{
				Name: "test_dir_one",
				Files: []model.FileTemplate{
					{
						Name: "test_file",
					},
				},
				Directories: []model.DirectoryTemplate{
					{
						Name: "inner_test_dir",
						Files: []model.FileTemplate{
							{
								Name: "inner_test_file",
							},
						},
					},
				},
			},
			{
				Name: "test_dir_two",
				Files: []model.FileTemplate{
					{
						Name:     "test_file",
						Template: testFileTemplate,
					},
					{
						Name: "another_test_file",
					},
				},
			},
		},
	}
})

var _ = AfterSuite(func() {
	os.RemoveAll(testDir)
})

var _ = Describe("Template Generator", func() {

	Describe("The template generator can write directory templates", func() {

		It("Doesn't fail", func() {
			Expect(usgen.WriteDirectoryTemplate(testTemplate, nil, testDir)).To(BeNil())
		})

		/*
			Expect:
			/tmp/usgen-<>/
				test_file
				test_test_dir_one/
					test_file
					inner_test_dir/
						inner_test_file
				test_test_dir_two/
					test_file
					another_test_file
		*/

		It("Creates the directories in the template", func() {
			var rootDir = fmt.Sprintf("%s/%s", testDir, "simple-test")
			Expect(rootDir).To(BeADirectory())
			Expect(rootDir + "/test_dir_one").To(BeADirectory())
			Expect(rootDir + "/test_dir_two").To(BeADirectory())
			Expect(rootDir + "/test_dir_one/inner_test_dir").To(BeADirectory())
		})

		It("Creates the files in the Template", func() {
			var rootDir = fmt.Sprintf("%s/%s", testDir, "simple-test")
			Expect(rootDir + "/test_dir_one/" + "test_file").To(BeAnExistingFile())
			Expect(rootDir + "/test_dir_two/" + "test_file").To(BeAnExistingFile())
			Expect(rootDir + "/test_dir_two/" + "another_test_file").To(BeAnExistingFile())
			Expect(rootDir + "/test_dir_one/inner_test_dir/inner_test_file").To(BeAnExistingFile())
		})

		It("Writes the templates", func() {
			info, err := os.Stat(testDir + "/simple-test/test_dir_two/test_file")
			Expect(err).To(BeNil())
			Expect(info.Size()).Should(BeNumerically(">", 80))
		})
	})

	Describe("The template generator can read template directories", func() {
		It("Can load directory templates", func() {
			t, err := usgen.LoadDirectoryTemplateFromPath(testDirTemplatePath)
			Expect(err).To(BeNil())
			Expect(t.Name).To(BeEquivalentTo("simple-test"))
			Expect(t.Files).ToNot(BeEmpty())
			Expect(t.Directories).ToNot(BeEmpty())
		})

	})
})

// In order for 'go test' to run this suite, we need to create
// a normal test function and pass our suite to suite.Run
func TestTemplateGenerator(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Template Generator")
}
